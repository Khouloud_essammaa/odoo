# -*- coding: utf-8 -*-
from odoo import http
from odoo.addons.website.controllers.main import Website
import json
from pprint import pprint

class MyBlog(Website):

    @http.route('/', type='http', auth="public", website=True)
    def index(self, **kw):
        posts = http.request.env['blog.post'].search([], limit=3)

        values = {
            'blog_posts': posts,
        }

        return http.request.render("my_blog.my_blog_hompage_test", values)

    @http.route('/developpement-web', type='http', auth="public", website=True)
    def developpement_web(self, **kw):
        domain = [('blog_id', '=', 1)]
        posts = http.request.env['blog.post'].search(domain, limit=3)

        values = {
            'blog_posts': posts,
        }

        return http.request.render("my_blog.template_web", values)


    @http.route('/marketing-digital', type='http', auth="public", website=True)
    def marketing_digital(self, **kw):
        domain = [('blog_id', '=', 2)]
        posts = http.request.env['blog.post'].search(domain, limit=3)

        values = {
            'blog_posts': posts,
        }

        return http.request.render("my_blog.template_marketing_digital", values)

    @http.route('/consulting', type='http', auth="public", website=True)
    def consulting(self, **kw):
        domain = [('blog_id', '=', 7)]
        posts = http.request.env['blog.post'].search(domain, limit=3)

        values = {
            'blog_posts': posts,
        }

        return http.request.render("my_blog.template_consulting", values)

    @http.route('/mobile', type='http', auth="public", website=True)
    def mobile(self, **kw):
        domain = [('blog_id', '=', 4)]
        posts = http.request.env['blog.post'].search(domain, limit=3)

        values = {
            'blog_posts': posts,
        }

        return http.request.render("my_blog.template_mobile", values)

    @http.route('/design', type='http', auth="public", website=True)
    def design(self, **kw):
        domain = [('blog_id', '=', 3)]
        posts = http.request.env['blog.post'].search(domain, limit=3)

        values = {
            'blog_posts': posts,
        }

        return http.request.render("my_blog.template_design", values)