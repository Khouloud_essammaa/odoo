$( document ).ready(function() {
	//vars
	  const navbar = $('#custom-header');

	$('#publicationsx').owlCarousel( {
			loop: true,
			items: 3,
			margin: 30,
			autoplay: false,
			responsiveClass:true,
			nav:true,
			autoplayTimeout: 8500,
			smartSpeed: 450,
			navText: ['<img src="./assets/image/prev.png">','<img src="./assets/image/next.png">'],
			responsive: {
				0: {
					items: 1,
					dots:false
				},
				600: {
					items: 2,
					nav:false,
					dots:true
				},
				1170: {
					items: 3,
					dots:false
				}
			}

		});

	$('#publications').owlCarousel( {
			
		items:3,
		loop:true,
		margin:30,
		responsiveClass:true,
		nav:true,
		navText: ['<img src="./assets/image/prev.svg">','<img src="./assets/image/next.svg">'],
		responsive:{
				0:{
					items:1,
					nav:false,
					dots:true
				},
				600:{
					items:2,
					nav:false,
					dots:true
					},
				1200:{
					items:3,
					nav:false,
					dots:false
					}
				}
		});
    
    $('nav.mobile .toggle').on('click', function(e) {
		$('nav.mobile').toggleClass('open');
		$('.navbar-top-collapse').toggleClass('open');
		$('body').toggleClass('no-scroll');
	
  });

    $('nav.mobile .menu ul li a').on('click', function(e) {
		$('nav.mobile').removeClass('open');
		$('nav.mobile .menu').removeClass('open');
		$('body').removeClass('no-scroll');
	  });

	$( ".form-control" ).on('focus', function(e) {
		e.preventDefault();
	$(this).parent().addClass('filled');
	});

	$( ".form-control" ).on('blur', function(e) {
		e.preventDefault();
	$(this).parent().removeClass('filled');
	});

	$('#technology-logos').owlCarousel({
	loop: true,
	margin:30,
	autoplay:true,
	autoplayTimeout:1000,
	autoplayHoverPause:true,
	responsiveClass:true,
		nav:true,
	navText: ['<i class="fa fa-angle-left fa-2x" aria-hidden="true"></i>','<i class="fa fa-angle-right fa-2x" aria-hidden="true"></i>'],
	responsive:{
		0:{
			items:2,
			nav:true,
			dots:false
		},
		600:{
			items:4,
			nav:false,
			dots:true
		},
		1300:{
			items:6,
			nav:false,
			dots:false
		}
	}
	});

           

    
   
	//  $(this).scrollTop(0);

	$(window).scroll( navbarOnScroll);

	 function navbarOnScroll(e) {
		var header = $("#custom-header");
		var scroll = $(window).scrollTop();
		let wrapperTop = $('#wrapwrap').offset().top;
	
		if (scroll >= wrapperTop + 2) {
			header.addClass("on-scroll");
			header.css('top', wrapperTop + 'px');
		} else {
			header.removeClass("on-scroll");
			header.css('top', '0px');
		}
	 }

	 // BTN SHow MENU Click 
	 $('#custom-header .navbar-toggle').on('click', function(e) {
		 var menu = $('#custom-header .navbar-top-collapse');
		 var expanded = menu.hasClass('open');

		 if(expanded == false){
			navbar.addClass("open");
			menu.addClass("open");
			$(this).addClass("open");
			menu.slideDown(100);
		 }else{
			navbar.removeClass("open");
			menu.removeClass("open");
			$(this).removeClass("open");
			menu.slideUp(100);
		 }
		 
	 });



	 // Calls on load
	 navbarOnScroll();

	 var scrollSpy = function(e){
		 
		 e.preventDefault();
		 
		 var elm = $($(this).attr("href").replace('/', ''));
        if(elm.length > 0){
			var mobile_menu = $("#mobile-menu");
			var btn_show_menu = $('#btn-show-menu');

			if(btn_show_menu.attr('aria-expanded') == "true"){
				btn_show_menu.click();
			}

			// btn_show_menu.addClass('collapsed');
			mobile_menu.removeClass("open");

            $('html, body').animate({
                scrollTop: elm.offset().top + 1
			}, 1000);
			

		}
    };


  
    // Scrollspy
	// $('#top_menu li a').on('click', scrollSpy);
	$('a.btn-goto').on('click', scrollSpy);
	// $('a.main-btn').on('click', scrollSpy);


	var serviceContainers = $('#services .service-container');
	var animationsElms = document.querySelectorAll('#services .anim-box');
	var animations = [];

	animationsElms.forEach((el, index) => {
		var source = $(el).data("animationsource");
		console.log(source);
		var animation = bodymovin.loadAnimation({
			container: el, // Required
			path: source, // Required
			renderer: 'svg', // Required
			// loop: true, // Optional
			autoplay: true, // Optional
			name: "icons_" + index, // Name for future reference. Optional.
		  });
		  
		  
		  $(el).parents(".service-container").attr('data-animindex', index);

		  animations.push({
			  index: index,
			  animation: animation
		  });

	});
	// Animation
	

	serviceContainers.mouseenter(function(e) {
		  var $this = $(this);
		  var index = $this.data('animindex');

		  var anim = animations.filter(a => a.index === index);
		
		  if(anim.length > 0){
			// console.log(anim);
			
			anim[0].animation.stop(); 
			// anim[0].animation.goToAndStop(50, true); 
			anim[0].animation.play(); 

		  }
	  });


	var theForm = document.getElementById('theForm');
	if(theForm != undefined){
		new stepsForm( theForm, {
			onSubmit : function( form ) {
				var inputFormDiv = document.getElementById('theForm');
				var countInput = inputFormDiv.getElementsByClassName('input').length;
	
				if(countInput == 6) {
					classie.addClass( theForm.querySelector( '.simform-inner' ), 'hide-this' );
	
					$.ajax({
						url: location.origin + '/website_form/crm.lead',
						type: "POST",
						data: $(theForm).serialize(),
						dataType: 'json',
						processData: false,
						success: function(data){
	
							if(data.id != undefined){
								var messageEl = theForm.querySelector( '.final-message' );
								messageEl.innerHTML = 'Merci ! nous allons vous communiquer dans les plus proches délais.';
								classie.addClass( messageEl, 'show' );
							}
						},
						error: function(data){
								console.log(data);
						}
	
					});
	
				}
			}
		} );
	}
	
	// call method to split text
	Splitting();


	// INIT WOW JS
	new WOW({
		offset:       100,
		mobile:       false,//in mobile off
	}).init();


	// realisation
	$('.no-realisation .realisation-carousel:not(.global-carousel.owl-carousel)').owlCarousel( {
			
		items:3,
		loop:true,
		center:true,
		margin:30,
		responsiveClass:true,
		nav:true,
		navText: ['<img src="./assets/image/prev.svg">','<img src="./assets/image/next.svg">'],
		responsive:{
				0:{
					items:1,
					nav:false,
					dots:true
				},
				600:{
					items:2,
					nav:false,
					dots:true
					},
				1200:{
					items:3,
					nav:false,
					dots:false
					}
				}
		});
	
	$('.testimonials .testimonials-carousel').owlCarousel( {
			
		items:1,
		loop:true,
		margin:30,
		responsiveClass:true,
		nav:true,
		navText: ['<img src="./assets/image/prev.svg">','<img src="./assets/image/next.svg">'],
		});

		var carousels = $('.global-carousel.owl-carousel');

		carousels.each(function(index) {
			var $this = $(this);

			let columns = Number($this.data('columns'));
			let mobile_columns = Number($this.data('mobile_columns'));
			let tablet_columns = Number($this.data('tablet_columns'));

			$this.owlCarousel( {
			
				items:columns,
				loop:false,
				margin:30,
				responsiveClass:true,
				nav:true,
				autoplay: true,
				navText: ['<img src="./assets/image/prev.svg">','<img src="./assets/image/next.svg">'],
				responsive: {
					0: {
						items: mobile_columns,
						margin:10,
					},
					600: {
						items: tablet_columns,
					},
					991: {
						items: columns,
					}
				}
				});
		})
	
});




