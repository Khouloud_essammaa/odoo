# -*- coding: utf-8 -*-
{
    'name': "Digency Website",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Abdelkarim fares",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Themes',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'website'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/layout.xml',
        'views/pages.xml',
        'views/assets.xml',
        'views/homepage.xml',
        'views/web.xml',
        'views/digital.xml',
        'views/consulting.xml',
        'views/mobile.xml',
        'views/design.xml',
        'views/contact.xml',
        'views/snippets.xml',
        'views/blog.xml',
'views/update.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}